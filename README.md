# modelScans
A ruby framework for performing scans using [SARAH](https://sarah.hepforge.org)/[SPheno](https://spheno.hepforge.org) or [FlexibleSUSY](http://flexiblesusy.hepforge.org) and [HiggsBounds](http://higgsbounds.hepforge.org), [HiggsSignals](http://higgsbounds.hepforge.org), [micrOMEGAs](https://lapth.cnrs.fr/micromegas).

## Dependecies
Internaly the code uses the following ruby packages:

* [sqlite3](https://github.com/sparklemotion/sqlite3-ruby)
* [activerecords](http://guides.rubyonrails.org/active_record_basics.html)
* [parallel](https://github.com/grosser/parallel) 
* [inifile](https://github.com/twp/inifile)

The plotting scrips use [numo](https://github.com/ruby-numo/numo-gnuplot) interface to [gnuplot](www.gnuplot.info)

All of the packages can be installed using 
```
gem install [--use-install] name_of_the_gem
```

## Usage

### Command line arguments

#### getting help
The list of command line options can be displayed using `-h` or `--help` switches.

#### chosing the spectrum generator
The generator can be selected using the `-g` flag.
The code supports two generator: SPheno and FlexibleSUSY, selecteble through `-g SPheno` or `-g FlexibleSUSY`, respectively.

#### automatic parallelization
A scan can be performed in parallel. The number of cores is specified through
`-n number_of_cores` or `-np number_of_cores` command line options, e.g `-n 3` or `-np 3`.

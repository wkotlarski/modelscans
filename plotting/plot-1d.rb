require 'sqlite3'
require 'numo/gnuplot'
require 'optparse'

options = {}
OptionParser.new do |opts|
   opts.banner =
       "\nA command line utility for creating 1d plots from data stored in an SQLite database.\n
Basic usage:


Options:

"

   opts.on("-x X_COLUMN_NAME","Name of the SQLite column to put on the x axis") do |x|
      options[:x] = x
   end

   opts.on("-y Y_COLUMN_NAME","Name of the SQLite column to put on y axis") do |y|
      options[:y] = y
   end

   opts.on("--table-x","Name of the SQLite table containing column to put on x axis") do |tx|
      options[:tx] = tx
   end

   opts.on("--table-y","Name of the SQLite table containing column to put on y axis") do |ty|
      options[:ty] = ty
   end

   opts.on("-h", "--help", "Displays this help screen") do
      puts opts
      exit
   end
end.parse!

if ARGV.length > 1 then
   puts "To many command line arguments. One argument (+options) expected. Exiting"
   exit
end

if !File.exist?("#{ARGV[0]}") then
   puts "Error! No database named #{ARGV[0]} found. Exiting."
   exit
end

def f(name, options)
# read in database name from command line
db = SQLite3::Database.new name

stm = db.prepare "SELECT
   t2.#{options[:x]}, t2.#{options[:y]}
   from Parameter_points t2"
rs = stm.execute

xFS = []; yFS = [];
rs.each do |row|
   xFS.push(row[0])
   yFS.push(row[1])
end
puts yFS

stm.close if stm
db.close if db
[xFS, yFS]
end

xZ, yZ = f("Fig9_BottomLeft_MaximalMixing_Z.db", options)
xB, yB = f("Fig9_BottomLeft_MaximalMixing_Box.db", options)
xD, yD = f("Fig9_BottomLeft_MaximalMixing_Dipole.db", options)
xR, yR = f("Fig9_BottomLeft_MaximalMixing_ChargeRadius.db", options)


Numo.gnuplot do
   set title: "Fig. 9c"
   set xlabel:"m1"
   set ylabel:"|amplitude|"
   set yrange: "[1.4e-12:1.5e-10]"
   set logscale: 'y'
   plot [xR, yR, w:'lines', lt:"rgb 'blue'", lw: 2, title: 'charge radius'], [xD, yD, w:'lines', lt:"rgb 'red'", lw: 2, title: 'dipole'], [xZ,yZ, w:'lines', lt:"rgb 'brown'", lw: 2, title: 'Z penguin'], [xB,yB, w:'lines', lt:"rgb 'green'", lw: 2, title: 'boxes']
end

sleep 1000

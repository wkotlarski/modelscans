require 'sqlite3'
require 'numo/gnuplot'

# read in database name from command line
puts File.exist? "#{ARGV[0]}"
db = SQLite3::Database.new "#{ARGV[0]}"
puts File.exist? "#{ARGV[0]}"

stm = db.prepare "SELECT
   t2.#{ARGV[1]}, t2.#{ARGV[2]}, gm2, brMuEGamma, CRMuE
   from Parameter_points t2"
rs = stm.execute

xFS = []; yFS = []; gm2 = []; br = []; cr = [];
rs.each do |row|
   xFS.push(row[0])
   yFS.push(row[1])
   gm2.push(row[2])
   br.push(row[3])
   cr.push(row[4])
end

# stm.close if stm
# db.close if db

# read in database name from command line
# db = SQLite3::Database.new "../SPheno/#{ARGV[0]}"

# stm = db.prepare "SELECT
  #  t2.#{ARGV[1]}, t2.#{ARGV[2]}, t2.#{ARGV[3]}
  #  from Parameter_points t2"
# rs = stm.execute

# xSP = []; ySP = []; zSP = []
# rs.each do |row|
  #  xSP.push(row[0])
  #  ySP.push(row[1])
  #  zSP.push(row[2])
# end

# stm.close if stm
# db.close if db

Numo.gnuplot do
  set :termoption, :dash
  reset
  set view:'map'
  set contour:'base'
  unset :surface
  set title:"3D gnuplot demo - 2D contour projection of last plot"
  set dgrid3d:"100,100,1 splines"
  set :style, :line, 1, lt:2, lc_rgb:"red", lw:3
  set :style, :line, 2, lt:2, lc_rgb:"orange", lw:2
  set :style, :line, 3, lt:2, lc_rgb:"yellow", lw:3
  set :style, :line, 4, lt:2, lc_rgb:"green", lw:2
  splot [xFS, yFS, gm2, w:'l', ls:1], [xFS, yFS, br, ls:2], [xFS, yFS, cr, with:'l', ls:3]
end
sleep 5
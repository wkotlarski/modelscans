require 'optparse'

def get_options 
   options = {}
   optparse = OptionParser.new do |opts|
      opts.banner = "Usage: optparse1.rb [options] file1 file2"
   
      options[:verbose] = false
      opts.on( '-v', '--verbose', 'Output more information' ) do
         options[:verbose] = true
      end
   
      options[:j] = 1
      opts.on( '-j', '--cores=num_of_cores', 'The number of cores to use (default 1).' ) do |n|
         options[:j] = n.to_i
      end
   
      options[:n] = 1e+3
      opts.on( '-n', '--n=num_of_points', 'The number of points to use (default 10k).' ) do |n|
         options[:n] = n.to_i
      end

      options[:model] = nil
      opts.on('-m', '--model MODEL_NAME', 'Model dsad dd') do |m|
         if m=='mssm' 
            options[:model] = :MSSM
         elsif m == 'mrssm'
            options[:model] = :MRSSM
         else
            puts "Error: Uknown model #{m}"
            exit
         end
      end
   
     options[:generator] = nil
     opts.on('-g', '--spectrum-generator GENERATOR', 'Spectrum generator. Either SARAH/SPheno or FlexibleSUSY') do |g|
       if g=='SPheno' || g=='FlexibleSUSY'
         options[:generator] = g
       else
         puts "Error: Unknown spectrum generator '#{g}'. Supported values are SPheno and FlexibleSUSY"
         exit
       end
     end
   end
   optparse.parse!
   
   if options[:model] == nil
      puts "ERROR: Model not specified. Use -m or --model switch."
      exit
   end
   
   puts "Being verbose..." if options[:verbose]
   puts "INFO: Running model #{options[:model]} using #{options[:generator]} spectrum generator" if options[:model]
   puts "INFO: Generating grid with a total of #{options[:n]} points using #{options[:j]} thread(s)"
   return options
end
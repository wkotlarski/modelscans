require 'gimuby'
require 'gimuby/genetic/solution/function_based_solution'


require_relative 'cmd_line_options'
require_relative 'mrssm'
require 'securerandom'

      $options = get_options
# STEP 1: First we define a solution
class SampleProblemSolution < FunctionBasedSolution
  
   def evaluate
      m = 1000*@x_values[10]
      r = 0.1 + 10*@x_values[11] 
      theta = Math::PI/4.0
      param = {tanB: 1.1 + 60*@x_values[0], bmu: (1000*@x_values[1])**2,
         LamTU: -1.5 + 3*@x_values[2], LamSU: -1.5 + 3*@x_values[3],
            LamTD: -1.5 + 3*@x_values[4], LamSD: -1.5 + 3*@x_values[5],
         muU: 1000*@x_values[6], muD: 1000*@x_values[7],
            M1: 1000*@x_values[8], M2: 1000*@x_values[9],
         me211: 1.0/2.0*m*m*(1+r*r-(-1+r*r)*Math.cos(2*theta)), me222: 1.0/2.0*m*m*(1+r*r+(-1+r*r)*Math.cos(2*theta)), me212: -1.0/2.0*(-1+r*r)*m*m*Math.sin(2*theta),
         me233: 1e+6,
         ml212: 0, ml221: 0,

         # fixed parameters
         ml211: 1000**2, ml222: 1000**2, ml233: 1000**2, Bmu: 25000, M3: 1500,
         mRu2: 4.0E+06, mRd2: 4.9E+05,
         mO2: 1000**2, m3: 1500,
         mq2: 1E+06, mq233: 1.00E+06, mu2: 1E+06, mu233: 1.00E+06, md2: 1E+06, md233: 1.0E+06,
         :mS2 => 2000**2, mT2: 3000**2}
      point = MRSSM.new("run_01", param, $options)
      if point.valid?
         puts "mh1 = #{'%5.1f' % point.mh1}, mChi1 = #{'%5.1f' % point.mChi1}, mCha1 = #{'%5.1f' % point.mCha1}, br(mu -> e gamma) = #{point.brMuEGamma}, CR = #{point.CRMuE}"
         return 1e+10 if point.brMuEGamma < 1e-20
         return 1e+10 if point.CRMuE < 1e-20
         return 1e+10 if point.mChi1 < 100
         return 1e+10 if point.mCha1 < 100
         exit if point.brMuEGamma/point.CRMuE < 100 && point.brMuEGamma < 1e-13
         return point.brMuEGamma/point.CRMuE + point.brMuEGamma/1e-13
      else
         1e+10
      end
  end

  protected

  def get_x_value_min;        0.0 end
  def get_x_value_max;        1.0 end
  def get_dimension_number;   12  end

end


# STEP 2: Let's optimize it with an optimal population

factory = Gimuby.get_factory
factory.optimal_population = TRUE
# We inject a block that will provide solutions inside our population
optimizer = factory.get_population {next SampleProblemSolution.new}

count = 0
10000000.times do
  puts "step #{count}"
  count += 1
  optimizer.generation_step
   puts '[' + optimizer.get_best_solution.get_solution_representation.join(',') + ']'
   puts optimizer.get_best_fitness
end

puts '[' + optimizer.get_best_solution.get_solution_representation.join(',') + ']'
puts optimizer.get_best_fitness


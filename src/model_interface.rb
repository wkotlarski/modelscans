require 'inifile'
require 'csv'

# abstract base class for the models
# that also serves as their factory
class ModelFactory
   # PEWO
   attr_reader :mW, :S, :T, :U, :amu
   # checkmate
   attr_reader :cm_allowed, :cm_r, :cm_analysis, :cm_sr
   # LFV
   attr_reader :brMuEGamma, :CRMuE
   # micrOMEGAs

   FILE = IniFile.load ("#{__dir__}/../config.ini")
   PATHS = FILE["paths"]

   def initialize
      raise "Tried to initialize an abstract base class"
   end

   def self.setup_model(model, x, y, z)
      case model
      when :MRSSM then MRSSM.new(x,y,z)
      when :MSSM then MSSM.new(x,y,z)
      end
   end

   def write_input_file_to_disk (lines)
      File.open("#{@run_path}/SLHA.in.#{self.class}", 'w') do |csv|
         lines.each {|l| csv.write l.join(' ') + "\n"}
      end
   end

   def get_cm_total_xsection herwig_output_path

   end

   def get_cm_exclussion (result_file_path)
      cm_output = CSV.read(result_file_path, {col_sep: ' '})
      @cm_allowed = cm_output[-4][-1]
      @cm_r = cm_output[-3][-1]
      @cm_analysis = cm_output[-2][-1]
      @cm_sr = cm_output[-1][-1]
   end

   # run HiggsBounds and return 1 if a model is excluded
   def run_hb
      raise "Called HiggsBounds on a parameter point that didn't produce spectrum file" unless valid?
      puts "H/A: #{@number_of_neutral_higgses}, H+: #{@number_of_charged_higgses}"

      system "cd #{@run_path} && #{PATHS['higgsbounds']}/HiggsBounds LandH effC #{@number_of_neutral_higgses} #{@number_of_charged_higgses} ./ >> higgsbounds_terminal.out 2>&1"
      hb_output = CSV.read("#{@run_path}/HiggsBounds_results.dat", {:col_sep => " "})
      hb_output[-1][-2].to_f
   end

   # run HiggsSignals and return a p-value
   def run_hs
      raise "Called HiggsSignals on a parameter point that didn't produce spectrum file" unless valid?
      puts "H/A: #{@number_of_neutral_higgses}, H+: #{@number_of_charged_higgses}"

      system "cd #{@run_path} && #{PATHS['higgssignals']}/HiggsSignals latestresults peak 2 effC #{@number_of_neutral_higgses} #{@number_of_charged_higgses} ./  >> higgssignals_terminal.out 2>&1"
      hs_output = CSV.read("#{@run_path}/HiggsSignals_results.dat", {:col_sep => " ", :row_sep => :auto})

      hs_output[-1][-1].to_f
   end

end

require 'csv'
require 'fileutils'
require 'inifile'

require_relative 'slha_reader'
require_relative 'model_interface'
require_relative 'slha_reader'

class MRSSM < ModelFactory

   # MRSSM input parameters
   attr_writer :tanB,
      :LamSU, :LamSD, :LamTU, :LamTD,  
      :muU, :muD, :M1, :M2, 
      :Bmu, :mS2, :mT2, :mRu2, :mRd2, 
      :me2, :me233, :ml2, :ml233,
      :me212, :ml212,
      :M3, :mO2,
      :mq2, :mq233, :mu2, :mu233, :md2, :md233

   attr_reader :mh1, :mh2, :mA2, :mChi1, :mCha1, :mRho1,
      :lsp1_mass, :lsp1_name, :mRh1, :mRh2, :mRum, :mRdp,
      :o1h2, :o2h2, :Z_width_limit, :brZEMu, :brhEMu, :brMu3E, :mSe1, :mSe2


   # initialize values of parameters from key, value list
   def initialize(path, args, options)

      @run_path = path
      @number_of_neutral_higgses = 7
      @number_of_charged_higgses = 3

      args.each do |k,v| # (key, value) pairs
         instance_variable_set("@#{k}", v) unless v.nil?
      end

      run_generator options[:generator]
   end

   def create_input_file name
      if name == 'SPheno'
         lines = CSV.read("#{__dir__}/SPheno_MRSSM_template.in", {:col_sep => " "})
      elsif name == 'FlexibleSUSY'
         lines = CSV.read("#{__dir__}/FlexibleSUSY_MRSSM_template.in", {:col_sep => " "})
      else
         puts 'Error! Unknown spectrum generator'
         exit
      end
      lines = lines.to_a.each {|row| row.delete_if {|el| el.nil?}}

      i = find_block_index(lines, 'MINPAR')
      lines[i+1][1] = @tanB

   if name == 'SPheno'
      i = find_block_index(lines, 'EXTPAR')
      [  @LamSD, @LamSU, @LamTD, @LamTU,
         @muD, @muU,
         @Bmu,
         @mRu2, @mRd2,
         @mO2,
         @M1, @M2, @M3,
         @mS2, @mT2
      ].each_with_index {|item, idx| lines[i+1+idx][1] = item}
   elsif name == 'FlexibleSUSY'
      i = find_block_index(lines, 'HMIXIN')
      [  @LamTD, @LamTU, @LamSD, @LamSU,
         @muD, @muU,
         @Bmu
      ].each_with_index {|item, idx| lines[i+1+idx][1] = item}

      i = find_block_index(lines, 'NMSSMRUNIN')
      lines[i+1][1] = @mS2

      i = find_block_index(lines, 'MSOFTIN')
      [
          @mT2,
         @mO2,
         @mRd2, @mRu2,
         @M1, @M2, @M3
      ].each_with_index {|item, idx| lines[i+1+idx][1] = item}
   end

      i = find_block_index(lines, 'MSE2IN')
      lines[i+1+0][2] = @me211
      lines[i+1+1][2] = @me212
      lines[i+1+3][2] = @me212
      lines[i+1+4][2] = @me222
      lines[i+1+8][2] = @me233

      i = find_block_index(lines, 'MSL2IN')
      lines[i+1+0][2] = @ml211
      lines[i+1+1][2] = @ml212
      lines[i+1+3][2] = @ml212
      lines[i+1+4][2] = @ml222
      lines[i+1+8][2] = @ml233

      i = find_block_index(lines, 'MSD2IN')
      lines[i+1+0][2] = @md2
      lines[i+1+4][2] = @md2
      lines[i+1+8][2] = @md233

      i = find_block_index(lines, 'MSU2IN')
      lines[i+1+0][2] = @mu2
      lines[i+1+4][2] = @mu2
      lines[i+1+8][2] = @mu233

      i = find_block_index(lines, 'MSQ2IN')
      lines[i+1+0][2] = @mq2
      lines[i+1+4][2] = @mq2
      lines[i+1+8][2] = @mq233

      write_input_file_to_disk (lines)
   end

   def run_generator name

      create_input_file name

      # run
      if name == 'SPheno'
         FileUtils.rm("#{@run_path}/SPheno.spc.MRSSM") if File.exist?("#{@run_path}/SPheno.spc.MRSSM")
         system "cd #{@run_path} && #{PATHS['spheno']}/bin/SPhenoMRSSM SLHA.in.MRSSM > /dev/null 2>&1"
      elsif name == 'FlexibleSUSY'
         FileUtils.rm("#{@run_path}/FlexibleSUSY.MRSSM") if File.exist?("#{@run_path}/FlexibleSUSY.MRSSM")
         system "cd #{@run_path} && #{PATHS['flexiblesusy']}/models/MRSSM2/run_MRSSM2.x --slha-input-file=SLHA.in.MRSSM --slha-output-file=FlexibleSUSY.MRSSM" #> /dev/null 2>&1"
      end

      # if spectrum was generated
      if name == 'SPheno' && File.exist?("#{@run_path}/SPheno.spc.MRSSM")
         @valid = true
         @spectrum = CSV.read("#{@run_path}/SPheno.spc.MRSSM", {:col_sep => " "})
         @spectrum = @spectrum.to_a.each {|row| row.delete_if {|el| el.nil?}}
         i = find_block_index(@spectrum, 'MASS')

         @mh1 = @spectrum[i+23][1].to_f
         @mh2 = @spectrum[i+24][1].to_f
         @mh3 = @spectrum[i+25][1].to_f

         @mA2 = @spectrum[i+27][1].to_f

         @mRh1 = @spectrum[i+55][1].to_f
         @mRh2 = @spectrum[i+56][1].to_f
         @mRum = @spectrum[i+57][1].to_f
         @mRdp = @spectrum[i+58][1].to_f
         @mChi1 = @spectrum[i+55-8][1].to_f
         @mCha1 = @spectrum[i+55-8+4][1].to_f
         @mRho1 = @spectrum[i+55-8+6][1].to_f
         @mW = @spectrum[i+36][1].to_f


         # next point if it's not neutrino, senutrino or neutral R-Higgs
         @lsp1 = Array.new(57) { |l| [@spectrum[i+2+l][1].to_f, @spectrum[i+2+l][3]] }
         @lsp1.delete_if { |_, x|
            %w(VWm hh_1 hh_2 hh_3 hh_4 Ah_2 Ah_3 Ah_4 Hpm_2 Hpm_3 Hpm_4 VZ Fd_1 Fd_2 Fd_3 Fu_1 Fu_2 Fu_3 Fe_1 Fe_2 Fe_3 Rh_1 Rh_2 SRum SRdp).include? x
         }
         # sort acording to masses
         @lsp1_mass, @lsp1_name = @lsp1.min_by { |a, b| a}

         i = find_block_index(@spectrum, 'SPhenoLowEnergy')
         @T = @spectrum[i+1][1].to_f
         @S = @spectrum[i+2][1].to_f
         @U = @spectrum[i+3][1].to_f
         @amu = @spectrum[i+5][1].to_f

         i = find_block_index(@spectrum, 'FlavorKitLFV')
         @brMuEGamma = @spectrum[i+1][1].to_f
         @CRMuE = @spectrum[i+8][1].to_f
         @brMu3E = @spectrum[i+10][1].to_f
         @brZEMu = @spectrum[i+17][1].to_f
         @brhEMu = @spectrum[i+20][1].to_f
      elsif name == 'FlexibleSUSY' && File.exist?("#{@run_path}/FlexibleSUSY.MRSSM")
         @valid = true
         @spectrum = CSV.read("#{@run_path}/FlexibleSUSY.MRSSM", {:col_sep => " "})
         @brMuEGamma = @spectrum[@spectrum.index{|r| r[-1] == 'OBSERVABLES.Fe_to_Fe'}][1].to_f
      else
         @valid = false
      end
   end

   def create_flexiblesusy_input_file

   end

   def run_flexible

   end

   # tells if the parameter point is valid, that is
   # if the spectrum was successfully generated
   def valid?
      # puts "Valid MRSSM point? #{@valid}"
      @valid
   end

   # calculate relic densities for R=1 and R=2 DM candidates
   # and store them in o1h2 and o2h2 variables
   def run_micromegas
      raise "Called micrOMEGAs on a parameter point that didn't produce spectrum file" unless @valid

      # run MO
      FileUtils.rm('omg.out') if File.exist?('omg.out')
      system "cd #{@run_path} && #{PATHS['micromegas']} 1>> micrOMEGAs.out 2>&1"

      # parse MO output
      micromega_output = CSV.read("#{@run_path}/omg.out", {:col_sep => " "})
      @o1h2 = micromega_output[0][1].to_f
      # @o2h2 = micromega_output[2][1].to_f
      @Z_width_limit = micromega_output[1][1].to_i
   end

   def run_checkmate
      raise "Called CheckMate on a parameter point that didn't produce spectrum file" unless @valid

      # init herwig model and run herwig
      system "source #{PATHS['herwig7']}/bin/activate"
      system "slha2herwig ../mrssm_herwig_convert/FRModel.template #{@run_path}/SPheno.spc.MRSSM -o #{@run_path}/MRSSM_slha.model"
      # system 'slha2herwig ~/HEP-software/mathematica/SARAH-4.12.2/Output/MRSSM/EWSB/herwig_convert/FRModel.template SPheno.spc.MRSSM -o MRSSM_slha.model'
      system "cd #{@run_path} && Herwig read ~/Dropbox/Fizyka/MRSSM/QCD_MC_study/LHC-MRSSM.in"
      system "cd #{@run_path} && Herwig run LHC-MRSSM.run"

      # parse herwig output
      cm = CSV.read("#{@run_path}/LHC-MRSSM.out", {:col_sep => " "})
      # number w/o exponent
      exp = cm[14][6].scan(/[^e]+\d+$/)[0].to_i
      xsec = cm[14][6].scan(/\d+.\d+(?=\()/)[0].to_f * 10**exp * 1e+6
      puts "total cross-section #{xsec}"
      puts cm[14][6].scan(/\((\d*)\)/)[0][0].to_i

      # run checkmate
      checkmate_run_dir = "checkmate_run"
      system "cd #{@run_path} && #{PATHS['checkmate']}/bin/CheckMATE -n #{checkmate_run_dir} -a '13' -xs '#{xsec}*FB' -xse '10 %' -ev LHC-*.hepmc -oe overwrite -od . -q"
      get_cm_exclussion "#{@run_path}/#{checkmate_run_dir}/result.txt"
   end
end


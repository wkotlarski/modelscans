require 'csv'
require 'fileutils'
require 'inifile'

require_relative 'model_interface'
require_relative 'slha_reader'

class MSSM < ModelFactory

   attr_writer :tanB,
         :mu, :M1, :M2,
         :Bmu, :mS2, :mT2, :mRu2, :mRd2,
         :me2, :me233, :ml2, :ml233,
         :me212, :ml212,
         :M3, :mO2,
         :mq2, :mq233, :mu2, :mu233, :md2, :md233
      attr_reader :mh1, :mh2, :mA2, :mCha1,
         :lsp1_mass, :lsp1_name,
         :o1h2, :o2h2, :Z_width_limit, :brZEMu, :brhEMu, :brMu3E

   def initialize path, args, options

      @run_path = path
      @number_of_neutral_higgses = 3
      @number_of_charged_higgses = 1

      args.each do |k,v| # (key, value) pairs
         instance_variable_set("@#{k}", v) unless v.nil?
      end

      lines = CSV.read("#{__dir__}/SPheno_MSSM_template.in", {:col_sep => " "})

      i = lines.index( ['Block', 'MINPAR', '#', 'Input', 'parameters', nil] )
      lines[i+1][1] = @tanB

      i = lines.index( ['Block', 'EXTPAR', '#', 'Input', 'parameters', nil] )
      lines[i+1][1] = @mu
      lines[i+2][1] = @Bmu
      lines[i+3][1] = @mq2
      lines[i+4][1] = @mq233
      lines[i+5][1] = @ml2
      lines[i+6][1] = @ml233
      lines[i+7][1] = @mu2
      lines[i+8][1] = @mu233
      lines[i+9][1] = @md2
      lines[i+10][1] = @md233
      lines[i+11][1] = @me2
      lines[i+12][1] = @me233
      lines[i+13][1] = @M1
      lines[i+14][1] = @M2
      lines[i+15][1] = @M3
      # @lines[@i+26][1] = @ml212
      # @lines[@i+27][1] = @me212

      write_spheno_input_file_to_disk lines

      # run SPheno
      FileUtils.rm("#{@run_path}/SPheno.spc.MSSM") if File.exist?("#{@run_path}/SPheno.spc.MSSM")
      system "cd #{@run_path} && #{PATHS['spheno']}/bin/SPhenoMSSM SLHA.in.MSSM > /dev/null 2>&1"

      # if spectrum was generated
      if File.exist? "#{@run_path}/SPheno.spc.MSSM"
         @valid = true
         @spectrum = CSV.read("#{@run_path}/SPheno.spc.MSSM", {:col_sep => " "})
         i = @spectrum.index( ['Block', 'MASS', '#', 'Mass', 'spectrum'] )

         @mh1 = @spectrum[i+23][1].to_f
         @mh2 = @spectrum[i+24][1].to_f

         @mA2 = @spectrum[i+25][1].to_f

         @mChi1 = @spectrum[i+39][1].to_f
         @mCha1 = @spectrum[i+43][1].to_f
         @mW = @spectrum[i+30][1].to_f


         # next point if it's not neutrino, senutrino or neutral R-Higgs
         @lsp1 = Array.new(57) { |l| [@spectrum[i+2+l][1].to_f, @spectrum[i+2+l][3]] }
         @lsp1.delete_if { |_, x|
            %w(VWm hh_1 hh_2 Ah_2 Hpm_2 VZ Fd_1 Fd_2 Fd_3 Fu_1 Fu_2 Fu_3 Fe_1 Fe_2 Fe_3).include? x
         }
         # sort acording to masses
         @lsp1_mass, @lsp1_name = @lsp1.min_by { |a, b| a}

         i = @spectrum.index( ['Block', 'SPhenoLowEnergy', '#', 'low', 'energy', 'observables', nil] )
         @T = @spectrum[i+1][1].to_f
         @S = @spectrum[i+2][1].to_f
         @U = @spectrum[i+3][1].to_f
         @amu = @spectrum[i+5][1].to_f

         i = @spectrum.index( ['Block', 'FlavorKitLFV', '#', 'lepton', 'flavor', 'violating', 'observables', nil] )
         @brMuEGamma = @spectrum[i+1][1].to_f
         @CRMuE = @spectrum[i+4][1].to_f
         @brMu3E = @spectrum[i+10][1].to_f
         @brZEMu = @spectrum[i+17][1].to_f
         @brhEMu = @spectrum[i+20][1].to_f
      else
         @valid = false
      end
   end

   # tells if the parameter point is valid, that is
   # if the spectrum was successfully generated
   def valid?
      puts "Valid MSSM point? #{@valid}"
      @valid
   end

   def run_checkmate
      raise "Called CheckMate on a parameter point that didn't produce spectrum file" unless @valid

      # init herwig model and run herwig
      system "source #{PATHS['herwig7']}/bin/activate"
      system "slha2herwig ../mssm_herwig_convert/FRModel.template #{@run_path}/SPheno.spc.MSSM -o #{@run_path}/MSSM_slha.model"
      system "cd #{@run_path} && Herwig read ~/Dropbox/Fizyka/MRSSM/QCD_MC_study/LHC-MSSM.in && Herwig run LHC-MSSM.run"

      # parse herwig output
      cm = CSV.read("#{@run_path}/LHC-MSSM.out", {:col_sep => " "})
      # number w/o exponent
      exp = cm[14][6].scan(/[^e]+\d+$/)[0].to_i
      xsec = cm[14][6].scan(/\d+.\d+(?=\()/)[0].to_f * 10**exp * 1e+6
      puts "total cross-section #{xsec}"
      puts cm[14][6].scan(/\((\d*)\)/)[0][0].to_i

      # run checkmate
      checkmate_run_dir = "checkmate_run"
      system "cd #{@run_path} && #{PATHS['checkmate']}/bin/CheckMATE -n #{checkmate_run_dir} -a '13' -xs '#{xsec} FB' -xse '10 %' -ev LHC-MSSM.hepmc -oe overwrite -od . -q"
      get_cm_exclussion "#{@run_path}/#{checkmate_run_dir}/result.txt"
   end
end

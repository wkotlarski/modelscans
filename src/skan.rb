# for sqlite3 database
require 'sqlite3'
require 'active_record'
require 'parallel'
require 'progress_bar'

require_relative 'cmd_line_options'
require_relative 'mrssm'
require_relative 'mssm'

options = get_options

$mutex = Mutex.new

ActiveRecord::Base.establish_connection(
   :adapter => "sqlite3",
   :database => "MRSSM_muU_vs_muD.db"
)

ActiveRecord::Schema.define do
 create_table :Parameter_points do |t|

   t.column :M1, :decimal
   t.column :M2, :decimal 
   t.column :muU, :decimal
   t.column :muD, :decimal
   #t.column :mu, :decimal
   t.column :LamSU, :decimal
   t.column :LamTU, :decimal
   t.column :LamSD, :decimal
   t.column :LamTD, :decimal
   t.column :Bmu, :decimal
   t.column :tanB, :decimal
   t.column :mq2, :decimal
   t.column :mq233, :decimal
   t.column :mu2, :decimal
   t.column :mu233, :decimal
   t.column :md2, :decimal
   t.column :md233, :decimal
   t.column :ml2, :decimal
   t.column :ml212, :decimal
   t.column :ml233, :decimal
   t.column :ml211, :decimal
   t.column :ml222, :decimal
   t.column :me2, :decimal
   t.column :me212, :decimal
   t.column :me233, :decimal
   t.column :me211, :decimal
   t.column :me222, :decimal
   t.column :mRu2, :decimal
   t.column :mRd2, :decimal
   t.column :mO2, :decimal
   t.column :M3, :decimal
   t.column :mS2, :decimal
   t.column :mT2, :decimal
   t.column :m, :decimal
   #
   t.column :mh1, :decimal
   t.column :hb, :decimal
   t.column :hs, :decimal
   #
   t.column :mW, :decimal
   t.column :S, :decimal
   t.column :T, :decimal
   t.column :U, :decimal
   t.column :Z_width_limit, :integer
   #
   t.column :gm2, :decimal
   #
   t.column :brMuEGamma, :decimal
   t.column :CRMuE, :decimal
   t.column :brZEMu, :decimal
   #
   t.column :oh2, :decimal
   #
   t.column :cm_r, :decimal
   t.column :cm_analysis, :string
   t.column :cm_sr, :string
end unless ActiveRecord::Base.connection.data_source_exists?('Parameter_points')
end

class Parameter_point < ActiveRecord::Base
#  validates :LamTD, :presence => true, :numericality => true
end

bar = ProgressBar.new(options[:n])

$number_of_valid_points = 0;
Parallel.each((1..options[:n]).to_a, in_threads: options[:j]) do |mass|
   muD = 300 #rand(100.0..1000.0)
   m1 = rand(0..250.0)
   m = 300
   lamSD = rand(-3.0..3.0)
   # target ratio of slepton masses
   r = 1.5
    #theta = Math::PI/4.0
   #theta = Math::PI/12.0
   theta = Math::PI/62.7
   params = {
      tanB: 3, Bmu: 25000,
      LamSD: lamSD, LamSU: 1e-5, LamTD: 1e-5, LamTU: 1e-5,
      M1: m1, M2: 8000, M3: 1500.0,
      muU: 8000, muD: muD,
      mS2: 2000**2, mT2: 3000**2,
      #mu: rand_mu,
      # ---------------------------
      m: m,
      # left sleptons
      ml211: (1e+4)**2, ml222: (1e+4)**2, ml212: 0, ml233: (1e+4)**2, 
      # right sleptons
      me211: 1.0/2.0*m*m*(1+r*r-(-1+r*r)*Math.cos(2*theta)), me222: 1.0/2.0*m*m*(1+r*r+(-1+r*r)*Math.cos(2*theta)), me233: (1e+4)**2, me212: -1.0/2.0*(-1+r*r)*m*m*Math.sin(2*theta),
      # ----------------------------
      # left squarks
      mq2: 1000**2, mq233: 1000**2, 
      # right d-squarks
      md2: 1000**2, md233: 1000**2,
      # right u-squark
      mu2: 1000**2, mu233: 1000**2,
      mRu2: 2000**2, mRd2: 2000**2,
      mO2: 1000**2
   }
   run_dir = "run_#{lamSD}_#{m1}"
   Dir.mkdir run_dir
   point = ModelFactory::setup_model(options[:model], run_dir, params, options)

   
   if point.valid? then
      #point.run_dm
      # point.run_checkmate
      # puts "CheckMate: #{point.cm_allowed}, r = #{point.cm_r}, analysis #{point.cm_analysis}, SR #{point.cm_sr}"
      # puts "#{point.mh1}, #{point.mW}, #{point.amu}, #{point.brMuEGamma}, #{point.CRMuE}" #", #{point.o1h2}, #{point.o2h2}, #{point.lsp1_name}, #{x1}, #{point.lsp1_mass}, #{y1}"

      # merge parameters with predictions
      out = params.merge(
                   CRMuE:   point.CRMuE,
         #mh1: point.mh1, #hb: point.run_hb, hs: point.run_hs,
         #mW: point.mW, S: point.S, T: point.T, U: point.U, #Z_width_limit: point.Z_width_limit,
         gm2: point.amu,
         brMuEGamma: point.brMuEGamma#, CRMuE: point.CRMuE, brZEMu: point.brZEMu#,
      #oh2: point.o1h2
         # CheckMate
         # cm_r: point.cm_r, cm_analysis: point.cm_analysis, cm_sr: point.cm_sr
      )
      # write to database
      $mutex.synchronize do
        $number_of_valid_points += 1
         ActiveRecord::Base.connection_pool.with_connection  do
            Parameter_point.create(out).valid?
         end
      end
   end

      $mutex.synchronize do
        bar.increment!
      end
end
puts "INFO: Generated #{$number_of_valid_points}/#{options[:n]} (#{100.*$number_of_valid_points.to_f/options[:n].to_f}%) valid points"
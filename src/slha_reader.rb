# Finds the index of a block withing list
#
# @return [Integer]
# @param [Object] file_content
# @param [Object] block_name
def find_block_index file_content, block_name
   idx = file_content.index{|r| r[0] == 'Block' && r[1] == block_name}
   if idx == nil
      puts "ERROR: Cannot find a #{block_name} block. This should have never happened. Exiting."
      exit
   else
      idx
   end
end

class SLHAReader
end
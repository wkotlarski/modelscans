require 'minitest/autorun'
require 'csv'
require_relative '../src/slha_reader'

class TestSLHAReader < Minitest::Test
  def test_csv_gem
    data = CSV.parse('a  bb   ccc', {col_sep: ' '})
    assert_equal [['a', 'bb', 'ccc']], data.to_a.each {|row| row.delete_if {|el| el.nil?}}
  end

  def test_find_block_index
   lines = CSV.read("test/SPheno.spc.MRSSM", {:col_sep => ' '})
   assert_equal 174, find_block_index(lines, 'MASS')
   assert_equal 667, find_block_index(lines, 'FlavorKitLFV')
  end

end
